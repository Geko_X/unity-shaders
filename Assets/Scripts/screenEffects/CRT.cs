﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CRT : MonoBehaviour {

	public Material material;
	//Material _material;

	void Awake() {
		//_material = new Material(material);
	}

	void OnRenderImage(RenderTexture source, RenderTexture destination) {
		Graphics.Blit(source, destination, material);

	}
}
