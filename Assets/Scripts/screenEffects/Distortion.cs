﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Distortion : MonoBehaviour {

	[Range(0.001f, 0.1f)]
	public float strength = 0.1f;

	public Material material;
	//Material _material;

	void Awake() {
		//_material = new Material(material);
	}

	void OnRenderImage(RenderTexture source, RenderTexture destination) {
		material.SetFloat("_distortionStrength", strength);
		Graphics.Blit(source, destination, material);

	}
}
