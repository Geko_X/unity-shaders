﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Greyscale : MonoBehaviour {

	[Range(0, 1)]
	public float intensity = 0;

	private Material material;

	// Use this for initialization
	void Awake() {

		material = new Material(Shader.Find("Hidden/Greyscale"));

	}

	void OnRenderImage(RenderTexture source, RenderTexture destination) {

		material.SetFloat("_bwBlend", intensity);
		Graphics.Blit(source, destination, material);
		
	}
}
