﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssembleObject : MonoBehaviour {

	public Material material;

	public float minY = 0;
	public float maxY = 2;

	public float duration = 5f;

	void Start() {
		StartCoroutine(doAnimation());	
	}

	IEnumerator doAnimation() {

		float y = minY;

		while(y <= maxY) {
			y += Time.deltaTime / duration;

			float val = Mathf.Lerp(minY, maxY, y);
			material.SetFloat("_ConstructorY", val);
			yield return null;
		}

		y = maxY;

		while (y >= minY) {
			y -= Time.deltaTime / duration;

			float val = Mathf.Lerp(minY, maxY, y);
			material.SetFloat("_ConstructorY", val);
			yield return null;
		}

		StartCoroutine(doAnimation());

	}

}
