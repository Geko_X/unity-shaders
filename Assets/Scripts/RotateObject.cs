﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

	public Vector3 speed = new Vector3(-20, 40, 10);

	// Update is called once per frame
	void Update () {

		transform.Rotate(speed * Time.deltaTime);

	}
}
