﻿Shader "Hidden/CRTDiffuse" {
	
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_MaskTex("Mask Texture", 2D) = "white" {}
		_maskBlend("Mask Blending", Range(0, 1)) = 0.5
		_maskSize("Mask Size", Float) = 1

		_scanSpeed("Scan Speed", Range(0.001, 3)) = 1
	}

	SubShader {
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass {

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			sampler2D _MaskTex;
			

			float _maskBlend;
			float _maskSize;
			float _scanSpeed;
			

			fixed4 frag (v2f_img i) : SV_Target {
				
				fixed4 color;

				// Scan lines

				float y = _Time[0] * _scanSpeed;
				float2 yPos = float2(0, y);

				fixed4 mask = tex2D(_MaskTex, (i.uv + yPos) * _maskSize);
				fixed4 blend = tex2D(_MainTex, i.uv);

				return lerp(mask, blend, _maskBlend);

			}

			ENDCG
		}
	}
}
