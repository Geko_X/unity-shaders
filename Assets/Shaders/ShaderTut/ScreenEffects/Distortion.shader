﻿Shader "Hidden/Distortion"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		_DistortionTex("Distortion", 2D) = "bump" {}
		_distortionStrength("Distortion Strength", Range(0.001, 0.1)) = 0.085
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			sampler2D _DistortionTex;
			
			float _distortionStrength;

			fixed4 frag (v2f_img i) : SV_Target {
				
				half2 n = tex2D(_DistortionTex, i.uv);
				half2 d = n * 2 - 1;		// Normalise from (0 -> 1) to (-1 -> 1)

				i.uv += d * _distortionStrength;
				i.uv = saturate(i.uv);

				return tex2D(_MainTex, i.uv);
			}
			
			ENDCG
		}
	}
}
