﻿
Shader "Custom/Water" {

	Properties {
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_CausticTex("Caustic", 2D) = "white" {}

		_Color("Color", Color) = (1, 1, 1, 1)
		_Period("Period", Range(0.001, 50)) = 1
		_offset("Offset", Range(0.001, 10)) = 1

		_Noise("Noise", 2D) = "bump" {}
		_Noise2("Noise2", 2D) = "bump" {}

		_Magnitude("Magnitude", Range(0, 1)) = 0.05
	}

	SubShader {
		Tags {
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}

		ZWrite On Lighting Off Cull Off Fog {Mode Off} Blend One Zero

		GrabPass {"_GrabTexture"}

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _GrabTexture;

			sampler2D _MainTex;
			sampler2D _Noise;
			sampler2D _Noise2;
			sampler2D _CausticTex;

			fixed4 _Color;
			float _Magnitude;
			float _Period;
			float _offset;

			float2 sinusoid(float2 x, float2 m, float2 M, float2 periodo) {

				float2 e = M - m;
				float2 coef = 3.1415 * 2.0 / periodo;

				return coef / 2.0 * (1 + sin(x * coef)) + m;
			}

			// Vert IN
			struct vin {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};

			// Vert to frag
			struct v2f {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;

				float4 pos_world : TEXCOORD2;
				float4 grabUV : TEXCOORD3;
			};

			// Vert
			v2f vert(vin v) {

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex); // Covert to world space
				
				o.color = v.color;
				o.texcoord = v.texcoord;
				o.texcoord1 = v.texcoord1;

				o.pos_world = mul(unity_ObjectToWorld, v.vertex);
				o.grabUV = ComputeGrabScreenPos(o.vertex);

				return o;

			}

			// Frag
			half4 frag(v2f i) : COLOR {

				fixed4 noise = tex2D(_Noise, i.texcoord);
				fixed4 noise2 = tex2D(_Noise2, i.texcoord);

				fixed4 color = tex2D(_MainTex, i.texcoord1);

				float time = _Time[1];

				float waterDispl = sinusoid(float2(time, time) + (noise.xy) * _offset + (noise2.xy) * _offset,
											float2(-_Magnitude, -_Magnitude),
											float2(_Magnitude, _Magnitude),
											float2(_Period, _Period)
									);

				i.grabUV.xy += waterDispl;

				fixed4 col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.grabUV));
				fixed4 causticCol = tex2D(_CausticTex, i.texcoord.xy * 0.25 + waterDispl * 5);

				col *= tex2D(_Noise, i.texcoord.xy * 0.25 + waterDispl * 5);

				return col * color * _Color * causticCol;

			}

			ENDCG
		}
	}
}
