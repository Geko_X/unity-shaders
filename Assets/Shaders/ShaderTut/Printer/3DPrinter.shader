﻿
Shader "Custom/3DPrinter" {
	Properties {

		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_ConstructorY("Constructor Y Pos", Float) = 1.5
		_ConstructionGap("Constructor Gap", Float) = 0.3
		_ConstructorColor("Constructor Color", Color) = (1, 0.5, 0, 1)
		_InnerColor("Inner Color", Color) = (1, 0, 0, 1)

	}


	SubShader {
		Tags { "RenderType"="Opaque" }

		LOD 200
		Cull Off
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Custom fullforwardshadows
		#include "UnityPBSLighting.cginc"

		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float3 viewDir;
		};

		sampler2D _MainTex;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float _ConstructorY;
		float _ConstructionGap;
		fixed4 _ConstructorColor;
		fixed4 _InnerColor;

		int building;
		float3 viewDir;

		// Surface function
		void surf (Input IN, inout SurfaceOutputStandard o) {

			float s = sin((IN.worldPos.x * IN.worldPos.z) * 60 + _Time[3] + o.Normal) / 120;

			if(IN.worldPos.y > _ConstructorY + s + _ConstructionGap) {
				discard;
			}

			viewDir = IN.viewDir;

			// Below the line
			if(IN.worldPos.y < _ConstructorY) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Alpha = c.a;
				building = 0;
			}

			// Above the line
			else {
				o.Albedo = _ConstructorColor.rgb;
				o.Alpha = _ConstructorColor.a;
				building = 1;
			}

			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

		}

		// Below the line, use standard lighting. Above, use unlit
		inline half4 LightingCustom(SurfaceOutputStandard s, half3 lightDir, UnityGI gi) {
			if(building) {
				return _ConstructorColor;
			}

			if(dot(s.Normal, viewDir) < 0)
				return _InnerColor;

			return LightingStandard(s, lightDir, gi);
		}

		inline void LightingCustom_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi) {
			LightingStandard_GI(s, data, gi);
		}

		ENDCG
	}
	FallBack "Diffuse"
}
