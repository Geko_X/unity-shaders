﻿
//https://www.alanzucconi.com/2015/06/17/surface-shaders-in-unity3d/

Shader "Custom/DiffuseTexture" {
	
	Properties {
	
		_MainTex ("Texture", 2D) = "white" {}

	}

	SubShader {
	
		Tags {
			"RenderType" = "Opaque"
		}

		CGPROGRAM

		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		}

		ENDCG
	}

	Fallback "Diffuse"

}
