﻿
//https://www.alanzucconi.com/2015/06/17/surface-shaders-in-unity3d/

Shader "Custom/SimpleDiffuse" {
	
	Properties {
	
		_color("Color", Color) = (1, 1, 1, 1)

	}

	SubShader {
	
		Tags {
			"RenderType" = "Opaque"
		}

		CGPROGRAM

		#pragma surface surf Lambert

		float4 _color;

		struct Input {
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = _color.rgb;
		}

		ENDCG
	}

	Fallback "Diffuse"

}
