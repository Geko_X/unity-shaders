﻿
//https://www.alanzucconi.com/2015/06/17/surface-shaders-in-unity3d/

Shader "Custom/DiffuseDistance" {
	
	Properties {
	
		_MainTex ("Texture", 2D) = "white" {}
		_Center ("Center", Vector) = (0, 0, 0)
		_Radius ("Radius", Float) = 0.5
		_Color ("Color", Color) = (1, 1, 1, 1)

	}

	SubShader {
	
		Tags {
			"RenderType" = "Opaque"
		}

		CGPROGRAM

		#pragma surface surf Lambert

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
		};

		sampler2D _MainTex;
		float3 _Center;
		float _Radius;
		float4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {
			float d = distance(_Center, IN.worldPos);
			float dN = 1 - saturate(d / _Radius);
 
			// IF-THEN-ELSE is bad in shaders

			/*
			if (dN > 0.25 && dN < 0.3)
				o.Albedo = _Color;
			else
				o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
			*/

			// Equivalent non-branching

			dN = step(0.25, dN) * step(dN, 0.3);
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * (1 - dN) + _Color * dN;

		}

		ENDCG
	}

	Fallback "Diffuse"

}
