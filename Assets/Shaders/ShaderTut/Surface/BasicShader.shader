﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


//https://www.alanzucconi.com/2015/06/10/a-gentle-introduction-to-shaders-in-unity3d/

Shader "Custom/BasicShader" {
	
	// Properties which are exposed in the inspector.
	//
	// These also need to be set in the subshader

	Properties {

		// Textures have the 2D type
		_texture("Texture", 2D) = "white" {}
		_normalMap("Normal Map", 2D) = "bump" {}

		// Numbers
		_int("Integer", Int) = 2
		_float("Float", Float) = 1.5
		_range("Range", Range(0.0, 1.0)) = 0.5

		// Struct things
		_color("Color", Color) = (1, 0, 0, 1)		// Color is in RGBA
		_vector("Vector", Vector) = (0, 0, 0, 0)	// Vectors are in XYZW

	}
	SubShader {

		// Tags let Unity know some info about this shader
		Tags {
			"RenderType" = "Opaque"
			"Queue" = "Geometry"
		}
		
		// Vert and Frag needs a pass
		//Pass {

			CGPROGRAM

			// Pragma data here

			// This creates a surface shader, using a Lambert lighting method
			#pragma surface surf Lambert

			// This creates the vertex and fragment shader
		
			// #pragma vertex vert fragment frag

			// Set the properties here.
			// These need to have the exact name as in the Properties block

			// Float4 is a collection of 4 numbers of 32 bits
			// Half4 is a collection of 4 numbers of 16 bits

			// Textures are called sampler2D here
			sampler2D _texture;
			sampler2D _normal;

			int _int;
			float _float;
			float _range;

			half4 _color;
			float4 _vector;

			struct Input {
				float2 uv_MainTex;
			};

			//struct vertInput {
			//	float4 pos : POSITION;
			//};

			//struct vertOutput {
			//	float 4 pos : SV_POSITION;
			//};

			// Surf function which we told Unity to use in the #pragma block
			void surf (Input IN, inout SurfaceOutput o) {
				o.Albedo = tex2D (_texture, IN.uv_MainTex).rgb;
			}

			//vertOutput vert(vertInput input) {
			//	vertOutput.o;
			//	o.pos = UnityObjectToClipPos(input.pos);
			//}

			//half4 frag(vertOutput output) : COLOR {
			//	return _color;
			//}

			ENDCG
		}
		Fallback "Diffuse"
	//}
}
