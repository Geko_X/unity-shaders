﻿
Shader "Custom/StandardSeeThroughWalls" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Bump("Bump", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_OutlineColor("See Through Color", Color) = (1, 1, 1, 1)
		_OutlineWidth("Outline Width", Range(0, 1)) = 0.005
		_OutlineExtrusion("Outline Extrusion", Range(0.5, 1.5)) = 1.1

	}

	SubShader {

//========================================================================================

		Pass {

			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Greater
			ZWrite Off

			Name "Behind"
			Tags{
				"RenderType" = "transparent"
				"Queue" = "transparent"
			}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 _OutlineColor;
			float _OutlineWidth;
			float _OutlineExtrusion;
			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			struct vin {
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;

				float4 vertex : TEXCOORD1;
			};

			v2f vert(vin v) {

				v2f o;
				
				o.pos = UnityObjectToClipPos(v.vertex * _OutlineExtrusion);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.color = v.color;

				return o;

			}

			half4 frag(v2f i) : COLOR {

				half4 c = _OutlineColor;
				float x = abs(i.uv.x - 0.5);
				float y = abs(i.uv.y - 0.5);

				float coord = step(1 - _OutlineWidth, abs(x)) || step(1 - _OutlineWidth, abs(y));

				c.a = lerp(0, _OutlineColor.a, coord);

				//c = lerp((0, 0, 0, 0), c, 1);

				return c;
			}

			ENDCG
		}

//========================================================================================
		// Normal Surface shader for when infront of something

		Name "Normal"
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Bump;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Bump;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;

			o.Normal = UnpackNormal(tex2D(_Bump, IN.uv_Bump));

		}
		ENDCG

	}

	FallBack "Diffuse"
}
