﻿Shader "Hidden/GunSightsColor" {
	
	Properties {

		_Color ("Color", Color) = (1, 0, 0, 1)
		_Alpha ("Base Alpha", Range(0, 1)) = 0.2
		_Alpha2("Ring Alpha", Range(0, 1)) = 1

		_Ring1("Outer Ring Location", Float) = 1
		_Ring2("Inner Ring Location", Float) = 0.5

		_Ring1Width("Outer Ring Width", Float) = 1
	}

	SubShader {

		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			fixed4 _Color;

			float _Alpha;
			float _Alpha2;

			float _Ring1;
			float _Ring2;

			float _Ring1Width;
			float _Ring2Width;


			// Vert IN
			struct vin {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			// Vert to frag
			struct v2f {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;

				float4 uvgrab : TEXCOORD1;
			};

			// Vert
			v2f vert(vin v) {

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex); // Covert to world space
				
				o.color = v.color;
				o.texcoord = v.texcoord;

				o.uvgrab = ComputeGrabScreenPos(o.vertex);
				return o;

			}

			int isCircle(float x, float y, float r, float width) {
				return (x*x + y*y == r*r);
			}

			// Frag
			half4 frag(v2f i) : COLOR {

				half4 baseColor = _Color;
				baseColor.a *= _Alpha;

				if(isCircle(i.texcoord.x, i.texcoord.y, _Ring1, _Ring1Width))
					return half4(1, 1, 1, 1);

				return baseColor;

			}

			ENDCG
		}
	}
}
