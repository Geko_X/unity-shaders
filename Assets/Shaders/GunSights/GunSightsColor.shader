﻿Shader "Hidden/GunSightsColor" {
	
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 0, 0, 1)
		_Alpha ("Base Alpha", Range(0, 1)) = 0.2
		_Alpha2("Ring Alpha", Range(0, 1)) = 1
	}

	SubShader {

		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;

			fixed4 _Color;

			float _Alpha;
			float _Alpha2;


			// Vert IN
			struct vin {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			// Vert to frag
			struct v2f {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;

				float4 uvgrab : TEXCOORD1;
			};

			// Vert
			v2f vert(vin v) {

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex); // Covert to world space
				
				o.color = v.color;
				o.texcoord = v.texcoord;

				o.uvgrab = ComputeGrabScreenPos(o.vertex);
				return o;

			}

			// Frag
			half4 frag(v2f i) : COLOR {

				half4 baseColor = _Color;
				baseColor.a *= _Alpha;

				half4 color = tex2D(_MainTex, i.texcoord);
				float a = color.a;

				color.a = _Alpha2;

				return lerp(baseColor, color * _Color, a);

			}

			ENDCG
		}
	}
}
